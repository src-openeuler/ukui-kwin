Name:           ukui-kwin
Version:        5.24.4
Release:        3
Summary:        KDE Window Manager for UKUI desktop environment
License:        GPL-2.0-or-later and LGPL-2.0-or-later
URL:            https://github.com/ukui/ukui-kwin     
Source0:        %{name}-%{version}.tar.gz

Patch01:        0001-fix-build-error-of-5.24.4.patch
#need Add blur strength for wayland.
#see https://gitee.com/openkylin/kwayland-server/commit/2b62e74e19276a1d5e3debd9b80209f86874e181
Patch02:        0002-fix-build-error-of-strength.patch

BuildRequires: plasma-breeze
BuildRequires: cmake
BuildRequires: python3
BuildRequires: extra-cmake-modules
BuildRequires: gettext
BuildRequires: kf5-kinit-devel
BuildRequires: kscreenlocker-devel
BuildRequires: libcap-devel
BuildRequires: libdrm-devel
BuildRequires: mesa-libEGL-devel
BuildRequires: libepoxy-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: mesa-libgbm-devel
BuildRequires: libICE-devel
BuildRequires: libinput-devel
BuildRequires: kdecoration-devel
BuildRequires: kf5-kactivities-devel
BuildRequires: kf5-kcompletion-devel
BuildRequires: kf5-kconfig-devel
BuildRequires: kf5-kconfigwidgets-devel
BuildRequires: kf5-kcoreaddons-devel
BuildRequires: kf5-kcrash-devel
BuildRequires: kf5-kdeclarative-devel
BuildRequires: kf5-kdoctools-devel
BuildRequires: kf5-kglobalaccel-devel
BuildRequires: kf5-ki18n-devel
BuildRequires: kf5-kiconthemes-devel
BuildRequires: kf5-kidletime-devel
BuildRequires: kf5-kcmutils-devel
BuildRequires: kf5-kio-devel
BuildRequires: kf5-knewstuff-devel
BuildRequires: kf5-knotifications-devel
BuildRequires: kf5-kpackage-devel
BuildRequires: kf5-plasma-devel
BuildRequires: kf5-krunner-devel
BuildRequires: kf5-kservice-devel
BuildRequires: kf5-ktextwidgets-devel
BuildRequires: kf5-kwayland-devel
BuildRequires: kf5-kwidgetsaddons-devel
BuildRequires: kf5-kwindowsystem-devel
BuildRequires: kf5-kxmlgui-devel
BuildRequires: kwayland-server-devel
BuildRequires: lcms2-devel
BuildRequires: pipewire-devel
BuildRequires: qaccessibilityclient-devel
BuildRequires: qt5-qtsensors-devel
BuildRequires: qt5-qtx11extras-devel
BuildRequires: libSM-devel
BuildRequires: systemd-devel
BuildRequires: wayland-devel
BuildRequires: libX11-devel
BuildRequires: libxcb-devel
BuildRequires: xcb-util-cursor-devel
BuildRequires: xcb-util-wm-devel
BuildRequires: xcb-util-image-devel
BuildRequires: xcb-util-keysyms-devel
BuildRequires: xcb-util-devel
BuildRequires: libXcursor-devel
BuildRequires: libXi-devel
BuildRequires: libxkbcommon-devel
BuildRequires: pkg-config
BuildRequires: plasma-wayland-protocols
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: qt5-qtscript-devel
BuildRequires: qt5-qttools-devel
BuildRequires: qt5-qtwayland-devel
BuildRequires: wayland-protocols-devel
BuildRequires: gsettings-qt-devel
BuildRequires: libXtst-devel


BuildRequires: kf5-kdbusaddons-devel >= 5.102.0
BuildRequires: kf5-kguiaddons-devel
BuildRequires: xorg-x11-server-Xwayland-devel
BuildRequires: qt5-qttools-static >= 5.15.2
BuildRequires: qt5-qtbase-static >= 5.15.2
BuildRequires: plasma-wayland-protocols-devel

Requires:      ukui-kwin-x11
Conflicts:     kwin

%description
Ukui-kwin  is the window manager for the UKUI3.0 Desktop. It gives you complete control over your windows, making sure they're not in the way but aid you in your task. It paints the window decoration, the bar on top of every window with (configurable) buttons like close, maximize and minimize. It also handles placing of windows and switching between them..
Transitional dummy for ukui-kwin-x11
This package is a transitional dummy to depend on the renamed ukui-kwin-x11 and can be removed.


%package -n ukui-kwin-wayland
Summary:        UKUI window manager, wayland version, PREVIEW release
Requires:       kwayland-integration libcap ukui-kwin-common ukui-kwin-wayland-backend-drm xorg-x11-server-Xwayland
Conflicts:      kwin-wayland
%description -n ukui-kwin-wayland
%{summary}.


%package -n ukui-kwin-wayland-backend-drm
Summary:        UKUI window manager drm plugin
Provides:       ukui-kwin-wayland-backend
%description -n ukui-kwin-wayland-backend-drm
%{summary}.


%package -n ukui-kwin-wayland-backend-fbdev
Summary:        UKUI window manager fbdev plugin
Provides:       ukui-kwin-wayland-backend
%description -n ukui-kwin-wayland-backend-fbdev
%{summary}.


%package -n ukui-kwin-wayland-backend-virtual
Summary:        UKUI window manager virtual plugin
Provides:       ukui-kwin-wayland-backend
%description -n ukui-kwin-wayland-backend-virtual
%{summary}.


%package -n ukui-kwin-wayland-backend-wayland
Summary:        UKUI window manager wayland plugin
Provides:       ukui-kwin-wayland-backend
%description -n ukui-kwin-wayland-backend-wayland
%{summary}.


%package -n ukui-kwin-wayland-backend-x11
Summary:        UKUI window manager x11 plugin
Provides:       ukui-kwin-wayland-backend
%description -n ukui-kwin-wayland-backend-x11
%{summary}.


%package -n ukui-kwin-common
Summary:        UKUI window manager, common files
Requires:       hwdata
Requires:       ukui-kwin-data
Requires:       libukui-kwineffects13
Requires:       kf5-kglobalaccel
Requires:       kf5-kirigami2
Requires:       kf5-kitemmodels
Requires:       kf5-kdeclarative
Requires:       qt5-qtmultimedia
#Recommends:     qt5-qtvirtualkeyboard
Conflicts:      kwin-common kwin-libs

%description -n ukui-kwin-common
KDE window manager, common files
 ukui-kwin (pronounced as one syllable "ukui-kwin") is the window
 manager for the KDE Plasma Desktop. It gives you complete
 control over your windows, making sure they're not in the way
 but aid you in your task. It paints the window decoration,
 the bar on top of every window with (configurable) buttons
 like close, maximize and minimize. It also handles placing
 of windows and switching between them.


%package -n ukui-kwin-data
Summary:        UKUI window manager data files
#Recommends:     kf5-plasma qt5-qtmultimedia qt5-qtquickcontrols qt5-qtvirtualkeyboard qt5-qtdeclarative
Conflicts:      kwin-common kwin-libs
%description -n ukui-kwin-data
%{summary}.


%package -n ukui-kwin-devel
Summary:        UKUI window manager - devel files
Requires:       mesa-libEGL-devel >= 1.2
Requires:       libepoxy-devel >= 1.3
Requires:       kf5-kconfig-devel >= 5.102.0
Requires:       kf5-kcoreaddons-devel >= 5.102.0
Requires:       kf5-kwindowsystem-devel >= 5.102.0
Requires:       qt5-qtx11extras-devel
Requires:       libxcb-devel >= 1.10
Requires:       qt5-qtbase-devel
Requires:       ukui-kwin-common
Conflicts:      kwin-devel
%description -n ukui-kwin-devel
%{summary}.


%package -n ukui-kwin-x11
Summary:        UKUI window manager, X11 version
Requires:       ukui-kwin-common libukui-kwinglutils13 libukui-kwinxrenderutils13
Provides:       ukui-kwin, x-window-manager
Conflicts:      kwin-x11
%description -n ukui-kwin-x11
%{summary}.


%package -n libukui-kwineffects13
Summary:        UKUI window manager effects library
Conflicts:      kwin-libs
%description -n libukui-kwineffects13
%{summary}.


%package -n libukui-kwinglutils13
Summary:        UKUI window manager gl utils library
Conflicts:      kwin-libs
%description -n libukui-kwinglutils13
%{summary}.


%package -n libukui-kwinxrenderutils13
Summary:        UKUI window manager render utils library
Conflicts:      kwin-libs
%description -n libukui-kwinxrenderutils13
%{summary}.


%prep
%autosetup -n %{name}-%{version} -p1

%build
%{cmake_kf5} -DBUILD_TESTING:BOOL=ON
%{cmake_build}


%install
%{cmake_install}

%posttrans -n ukui-kwin-x11
ln -s %{_bindir}/kwin_x11 %{_bindir}/kwin

%postun -n ukui-kwin-x11
unlink  /usr/bin/kwin


%files
%license debian/copyright

%files -n ukui-kwin-wayland
%{_bindir}/kwin_wayland
%{_bindir}/kwin_wayland_wrapper
%{_prefix}/lib/systemd/user/plasma-kwin_wayland.service

%files -n ukui-kwin-wayland-backend-drm
%{_kf5_qtplugindir}/org.kde.kwin.waylandbackends/KWinWaylandDrmBackend.so

%files -n ukui-kwin-wayland-backend-fbdev
%{_kf5_qtplugindir}/org.kde.kwin.waylandbackends/KWinWaylandFbdevBackend.so

%files -n ukui-kwin-wayland-backend-virtual
%{_kf5_qtplugindir}/org.kde.kwin.waylandbackends/KWinWaylandVirtualBackend.so

%files -n ukui-kwin-wayland-backend-wayland
%{_kf5_qtplugindir}/org.kde.kwin.waylandbackends/KWinWaylandWaylandBackend.so

%files -n ukui-kwin-wayland-backend-x11
%{_kf5_qtplugindir}/org.kde.kwin.waylandbackends/KWinWaylandX11Backend.so

%files -n ukui-kwin-common
%{_libexecdir}/kwin-applywindowdecoration
%{_libexecdir}/kwin_killer_helper
%{_libexecdir}/kwin_rules_dialog
%{_kf5_libdir}/kconf_update_bin/kwin5_update_default_rules
%{_libdir}/libkcmkwincommon.so.*
%{_libdir}/libkwin.so.*
%{_qt5_plugindir}/kcm_kwin*.so
%{_qt5_plugindir}/kcms/kcm_kwin*.so
%{_qt5_plugindir}/kcms/kcm_virtualkeyboard.so
%{_kf5_qtplugindir}/kpackage/packagestructure/*
%{_kf5_qtplugindir}/org.kde.kdecoration2/*.so
%{_kf5_qtplugindir}/kwin/*
%{_qt5_plugindir}/kwin*.so
%{_qt5_qmldir}/org/kde/kwin/*
%{_datadir}/touchgesture/touchgesture.xml

%files -n ukui-kwin-data
%{_datadir}/applications/org.kde.kwin_rules_dialog.desktop
%{_sysconfdir}/xdg/*
%{_datadir}/config.kcfg/kwin*.kcfg
%{_datadir}/config.kcfg/virtualdesktopssettings.kcfg
%{_datadir}/doc/*/*/kcontrol/*
%{_datadir}/icons/*/*/apps/kwin.*
%{_datadir}/kconf_update/*.sh
%{_datadir}/kconf_update/kwin-5.18-move-animspeed.py
%{_datadir}/kconf_update/*.upd
%{_datadir}/kconf_update/kwin-5.21-desktop-grid-click-behavior.py
%{_datadir}/kconf_update/kwin-5.21-no-swap-encourage.py
%{_datadir}/kconf_update/kwin-5.23-remove-cover-switch.py
%{_datadir}/kconf_update/kwin-5.23-remove-cubeslide.py
%{_datadir}/kconf_update/kwin-5.23-remove-flip-switch.py
%{_datadir}/kconf_update/kwin-5.23-remove-xrender-backend.py
%{_datadir}/kconf_update/kwinrules-5.19-placement.pl
%{_datadir}/kconf_update/kwinrules-5.23-virtual-desktop-ids.py
%{_datadir}/knotifications5/kwin.notifyrc
%{_datadir}/knsrcfiles/*.knsrc
%{_datadir}/kpackage/kcms/kcm_kwin*
%{_datadir}/kpackage/kcms/kcm_virtualkeyboard/contents/ui/main.qml
%{_datadir}/kpackage/kcms/kcm_virtualkeyboard/metadata.desktop
%{_datadir}/kpackage/kcms/kcm_virtualkeyboard/metadata.json
%{_datadir}/krunner/dbusplugins/kwin-runner-windows.desktop
%{_datadir}/kservices5/kcm_kwin_*.desktop
%{_datadir}/kservices5/kcm_kwinrules.desktop
%{_datadir}/kservices5/kcm_virtualkeyboard*.desktop
%{_datadir}/kservices5/kwin*.desktop
%{_datadir}/kservices5/kwin/*
%{_datadir}/kservicetypes5/kwin*.desktop
%{_datadir}/kwin/*
%{_datadir}/locale/*
%{_kf5_datadir}/qlogging-categories5/org_kde_kwin.categories

%files -n ukui-kwin-devel
%{_includedir}/kwin*.h
%{_libdir}/cmake/KWinDBusInterface/*
%{_libdir}/cmake/KWinEffects/*
%{_libdir}/libkwineffects.so
%{_libdir}/libkwinglutils.so
%{_libdir}/libkwinxrenderutils.so
%{_datadir}/dbus-1/interfaces/org.kde.KWin.*
%{_datadir}/dbus-1/interfaces/org.kde.kwin.*

%files -n ukui-kwin-x11
%{_bindir}/kwin_x11
%{_kf5_qtplugindir}/org.kde.kwin.platforms/KWinX11Platform.so
%{_prefix}/lib/systemd/user/plasma-kwin_x11.service

%files -n libukui-kwineffects13
%{_libdir}/libkwineffects.so.5.*
%{_libdir}/libkwineffects.so.13

%files -n libukui-kwinglutils13
%{_libdir}/libkwinglutils.so.5.*
%{_libdir}/libkwinglutils.so.13

%files -n libukui-kwinxrenderutils13
%{_libdir}/libkwinxrenderutils.so.5.*
%{_libdir}/libkwinxrenderutils.so.13


%changelog
* Thu Feb 27 2025 peijiankang <peijiankang@kylinos.cn> - 5.24.4-3
- remove Recommends

* Thu Nov 28 2024 huayadong <huayadong@kylinos.cn> - 5.24.4-2
- adopt to new cmake macro

* Fri Aug 30 2024 peijiankang <peijiankang@kylinos.cn> - 5.24.4-1
- Reinit version to 5.24.4

* Fri May 24 2024 peijiankang <peijiankang@kylinos.cn> - 5.27.5-1
- update version to 5.27.5

* Tue Aug 29 2023 peijiankang<peijiankang@kylinos.cn> - 1.0.5-7
- update qt5-qttools-devel to qt5-qttools-static

* Mon Aug 21 2023 peijiankang<peijiankang@kylinos.cn> - 1.0.5-6
- add 0003-fix-build-error-about-windowClass.patch

* Thu Jun 01 2023 peijiankang<peijiankang@kylinos.cn> - 1.0.5-5
- add Obsoletes ukui-kwin-libs

* Thu Mar 09 2023 peijiankang<peijiankang@kylinos.cn> - 1.0.5-4
- fix mate-terminal theme

* Thu Feb 16 2023 peijiankang<peijiankang@kylinos.cn> - 1.0.5-3
- fix ukui-kwin-data install error

* Tue Dec 6 2022 peijiankang<peijiankang@kylinos.cn> - 1.0.5-2
- modify install error

* Fri Dec 2 2022 peijiankang<peijiankang@kylinos.cn> - 1.0.5-1
- update version to 1.0.5

* Fri Jul 29 2022 peijiankang<peijiankang@kylinos.cn> - 1.0.4-5
- modify complie error of kde5.95

* Fri Apr 8 2022 peijiankang<peijiankang@kylinos.cn> - 1.0.4-4
- remove kwin requires

* Wed Mar 2 2022 peijiankang<peijiankang@kylinos.cn> - 1.0.4-3
- add kwin requires

* Fri Feb 25 2022 peijiankang<peijiankang@kylinos.cn> - 1.0.4-2
- update to version 1.0.4-2

* Fri Feb 11 2022 peijiankang<peijiankang@kylinos.cn> - 1.0.4-1
- Init ukui-kwin 1.0.4-1

